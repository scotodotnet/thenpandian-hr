﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="AttendDashBoard.aspx.cs" Inherits="AttendDashBoard" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<asp:UpdatePanel ID="UpdatePanel5" runat="server">
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			
			<!-- begin page-header -->
			<h1 class="page-header">DashBoard </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
		 <div class="row">
      <!-- Employee Strength Department Wise Start -->
        <div class="col-sm-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Employee Strength </h4>  
                </div>
                <div class="panel-body">
                        <div>
                            <asp:Chart ID="Employee_Strength_DeptWise" runat="server" Compression="100" 
                                EnableViewState="True" Width="480px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Doughnut" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                </div>
            </div>
        </div><!-- col-sm-6 -->
        <!-- Employee Strength Department Wise End -->
        
        <!-- Employee Attendance Department Wise Start -->
        <div class="col-sm-6">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Employee Attendance </h4>  
                </div>
                <div class="panel-body">
                        <div>
                            <asp:Chart ID="Employee_Attendance_DeptWise" runat="server" Compression="100" 
                                EnableViewState="True" Width="480px">
                                <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Pie" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                        </div>
                </div>
            </div>
        </div><!-- col-sm-6 -->
        <!-- Employee Attendance Department Wise End -->
        
        
    </div><!-- Row -->
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>




</asp:Content>

