﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptAdvaceEmpReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query;
    string mydate;
    string SQL = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesCode();
        }
       
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from ["+Session["Rights"]+"]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Load_WagesCode()
    {
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }
    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlMacineId.Items.Clear();
        Query = "Select convert(varchar(10), EmpNo)as EmpNo from Employee_Mst where Wages='" + ddlWagesType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlMacineId.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMacineId.DataTextField = "EmpNo";
        ddlMacineId.DataValueField = "EmpNo";
        ddlMacineId.DataBind();
    }
    protected void btnEmpAdv_Click(object sender, EventArgs e)
    {
        try
        {
            string ReportName = "Emp Wise Report";

            bool ErrFlag = false;

            if (ddlMacineId.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                ResponseHelper.Redirect("RptAdbanceShow.aspx?BName=" + ReportName + "&EmpNo=" + ddlMacineId.SelectedValue, "_blank", "");
            }

        }
        catch (Exception Ex)
        {
            
            throw;
        }
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        try
        {
            string ReportName = "Advance Report DateWise";
            string EmpType = "";
            bool ErrFlag = false;

            if ((ddlWagesType.SelectedValue == "0") || (ddlWagesType.SelectedValue == "-Select-"))
            {
                EmpType = "";
            }
            else
            {
                EmpType = ddlWagesType.SelectedItem.Text;
            }
                

            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                ResponseHelper.Redirect("RptAdbanceShow.aspx?BName=" + ReportName + "&EmpNo=" + ddlMacineId.SelectedValue + "&FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text  + "&EmpType=" + EmpType, "_blank", "");
            }

        }
        catch (Exception Ex)
        {

            throw;
        }
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
            return;
        }
        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            string SSQL = "";
            SSQL = "Select EM.MachineID as EmpNo,'' as Name,'0' as [Open],'0' as [PAID],'0' as [RECD],'0' as [Close] from Employee_Mst EM ";
            SSQL = SSQL + " inner join [" + SessionPayroll + "]..Advance_Credit_Debit AP on AP.MachineID=EM.MachineID ";
            SSQL = SSQL + "where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            if (ddlWagesType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " and Wages='" + ddlWagesType.SelectedItem.Text + "'";
            }
            if (ddlMacineId.DataSource != null)
            {
                if (ddlMacineId.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " and MachineID='" + ddlMacineId.SelectedItem.Text + "'";
                }
            }
            SSQL = SSQL + " and AP.Status='Pending' group by EM.MachineID";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SqlConnection cn = new SqlConnection(constr);
                    SqlCommand cmd;
                    cn.Open();

                    if (dt.Rows[i]["EmpNo"].ToString() == "2033")
                    {
                        string Stop = "";
                    }


                    SSQL = "";
                    SSQL = "Select (FirstName+' '+LastName) as Name from Employee_Mst where MachineID='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                    SSQL = SSQL + " and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    cmd = new SqlCommand(SSQL, cn);
                    dt.Rows[i]["Name"] = cmd.ExecuteScalar();


                    //For Open Balance
                    SSQL = "";
                    SSQL = "Select (isnull(sum(AmtAdd),'0')-isnull(sum(AmtMin),'0')) as [Open] from [" + SessionPayroll + "]..Advance_Credit_Debit where Ccode='" + SessionCcode + "'";
                    SSQL = SSQL + " and Lcode='" + SessionLcode + "' and Convert(Datetime,Transdate,103)<Convert(datetime,'" + txtFromDate.Text + "',103)";
                    SSQL = SSQL + " and MachineID='" + dt.Rows[i]["EmpNo"].ToString() + "' and Status='Pending' Group by MachineID";
                    cmd = new SqlCommand(SSQL, cn);
                    dt.Rows[i]["Open"] = cmd.ExecuteScalar();

                    //For Paid Amount
                    SSQL = "";
                    SSQL = "Select (isnull(sum(AmtAdd),'0')) as [Paid] from [" + SessionPayroll + "]..Advance_Credit_Debit where Ccode='" + SessionCcode + "'";
                    SSQL = SSQL + " and Lcode='" + SessionLcode + "' and Convert(Datetime,Transdate,103)>=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(Datetime,Transdate,103)<=Convert(datetime,'" + txtToDate.Text + "',103)";
                    SSQL = SSQL + " and MachineID='" + dt.Rows[i]["EmpNo"].ToString() + "' and Status='Pending' Group by MachineID";
                    cmd = new SqlCommand(SSQL, cn);
                    dt.Rows[i]["PAID"] = cmd.ExecuteScalar();

                    //For Received Amount
                    SSQL = "";
                    SSQL = "Select isnull(sum(AmtMin),'0') as [Recv] from [" + SessionPayroll + "]..Advance_Credit_Debit where Ccode='" + SessionCcode + "'";
                    SSQL = SSQL + " and Lcode='" + SessionLcode + "' and Convert(Datetime,Transdate,103)>=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(Datetime,Transdate,103)<=Convert(datetime,'" + txtToDate.Text + "',103)";
                    SSQL = SSQL + " and MachineID='" + dt.Rows[i]["EmpNo"].ToString() + "' and Status='Pending' Group by MachineID";
                    cmd = new SqlCommand(SSQL, cn);
                    dt.Rows[i]["RECD"] = cmd.ExecuteScalar();

                    //For Closing Amount
                    SSQL = "";
                    SSQL = "Select (isnull(sum(AmtAdd),'0')-isnull(sum(AmtMin),'0')) as [Close] from [" + SessionPayroll + "]..Advance_Credit_Debit where Ccode='" + SessionCcode + "'";
                    SSQL = SSQL + " and Lcode='" + SessionLcode + "' and Convert(Datetime,Transdate,103)>=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(Datetime,Transdate,103)<=Convert(datetime,'" + txtToDate.Text + "',103)";
                    SSQL = SSQL + " and MachineID='" + dt.Rows[i]["EmpNo"].ToString() + "' and Status='Pending' Group by MachineID";
                    cmd = new SqlCommand(SSQL, cn);
                    dt.Rows[i]["Close"] = cmd.ExecuteScalar();
                    cn.Close();
                    dt.AcceptChanges();
                }
                GVAdvance.DataSource = dt;
                GVAdvance.DataBind();
                int _rowCount = dt.Rows.Count + 1;

                string attachment = "attachment;filename=Advance.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                GVAdvance.RenderControl(htextw);
                //gvDownload.RenderControl(htextw);
                //Response.Write("Contract Details");
                Response.Write(stw.ToString());

                Response.Write("<table border='1'>");
                Response.Write("<tr>");
                Response.Write("<td colspan='2' align='right'>");
                Response.Write("TOTAL");
                Response.Write("</td>");
                Response.Write("<td >");
                Response.Write("=sum(C2:C" + _rowCount + ")");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("=sum(D2:D" + _rowCount + ")");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("=sum(E2:E" + _rowCount + ")");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("=sum(F2:F" + _rowCount + ")");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<table>");

                Response.End();
                Response.Clear();

            }
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}