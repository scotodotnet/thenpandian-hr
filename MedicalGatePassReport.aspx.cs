﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class MedicalGatePassReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    String CurrentYear1;
    static int CurrentYear;

    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {



            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Salary Cover Summary Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

                Load_TransNo();
            }



        }
    }
    private void Load_TransNo()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        ddltransNo.Items.Clear();
        query = "Select distinct Cast(TransID as varchar(20)) as TransID from Medical_GatePass where CCode='" + SessionCcode + "' And LCode='" + SessionLcode + "'";
       
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddltransNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["TransID"] = "-Select-";
        dr["TransID"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddltransNo.DataTextField = "TransID";
        ddltransNo.DataValueField = "TransID";
        ddltransNo.DataBind();
    }
   
    protected void btnReport_Click(object sender, EventArgs e)
    {

      
        
            ResponseHelper.Redirect("MedicalGatePassReportDisplay.aspx?TransNo=" + ddltransNo.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text, "_blank", "");
     
    }
}
