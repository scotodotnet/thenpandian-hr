﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstRecruitOfficer : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Recruitment Officer Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Unit();
        }

        Load_Data_RecruitOfficer();
    }

    private void Load_Data_RecruitOfficer()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstRecruitOfficer";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select LocCode from Location_Mst where CompCode='" + SessionCcode + "'";
        //query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LocCode"] = "-Select-";
        dr["LocCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "LocCode";
        ddlUnit.DataValueField = "LocCode";
        ddlUnit.DataBind();

    }


    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstRecruitOfficer where ROName='" + e.CommandName.ToString() + "' And Unit='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MstRecruitOfficer where ROName='" + e.CommandName.ToString() + "' And Unit='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Recruitment Officer Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_RecruitOfficer();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstRecruitOfficer where ROName='" + e.CommandName.ToString() + "' And Unit='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {

            txtROName.Text = DT.Rows[0]["ROName"].ToString();
            txtMobileNo.Text = DT.Rows[0]["Mobile"].ToString();
            ddlUnit.SelectedValue = DT.Rows[0]["Unit"].ToString();
            txtAddress.Text = DT.Rows[0]["Address"].ToString();

            btnSave.Text = "Update";
        }
        else
        {
            txtROName.Text = "";
            txtMobileNo.Text = "";
            ddlUnit.SelectedValue = "-Select-";
            txtAddress.Text = "";
            btnSave.Text = "Save";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();

        query = "select * from MstRecruitOfficer where ROName='" + txtROName.Text.ToUpper() + "' And Unit='" + ddlUnit.SelectedValue + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            SaveMode = "Update";
            query = "delete from MstRecruitOfficer where ROName='" + txtROName.Text.ToUpper() + "' And Unit='" + ddlUnit.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        query = "Insert into MstRecruitOfficer(ROName,Mobile,Unit,Address)";
        query = query + "values('" + txtROName.Text.ToUpper() + "','" + txtMobileNo.Text + "','" + ddlUnit.SelectedValue + "','" + txtAddress.Text + "')";
        objdata.RptEmployeeMultipleDetails(query);
        
        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Recruitment Officer Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Recruitment Officer Saved Successfully...!');", true);
        }

        Load_Data_RecruitOfficer();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtROName.Text = "";
        txtMobileNo.Text = "";
        ddlUnit.SelectedValue = "-Select-";
        txtAddress.Text = "";

        btnSave.Text = "Save";
    }
}
