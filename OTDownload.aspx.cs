﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
public partial class OTDownload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string Query = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedValue;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January": Month_Last = "01";
                    break;
                case "February": Month_Last = "02";
                    break;
                case "March": Month_Last = "03";
                    break;
                case "April": Month_Last = "04";
                    break;
                case "May": Month_Last = "05";
                    break;
                case "June": Month_Last = "06";
                    break;
                case "July": Month_Last = "07";
                    break;
                case "August": Month_Last = "08";
                    break;
                case "September": Month_Last = "09";
                    break;
                case "October": Month_Last = "10";
                    break;
                case "November": Month_Last = "11";
                    break;
                case "December": Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[0];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();

        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }


    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_TwoDates();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string PerDay = "0";
        string PerHour = "0";
        string TotalPay = "0";
        try
        {

            if (ddlMonths.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if ((txtdays.Text.Trim() == "") || (txtdays.Text.Trim() == null))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Working days in a Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //else if (Convert.ToInt32(txtdays.Text) == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days Properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //else if (txtFrom.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //else if (txtTo.Text.Trim() == "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
          
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                //int Month_Mid_Total_Days_Count = 0;
                //Month_Mid_Total_Days_Count = (int)((Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtFrom.Text)).TotalDays);
                //Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                //txtdays.Text = Month_Mid_Total_Days_Count.ToString();
                decimal total = 0;
                int total_check = 0;
                decimal days = 0;
                //string from_date_check = txtFrom.Text.ToString();
                //string to_date_check = txtTo.Text.ToString();
                //from_date_check = from_date_check.Replace("/", "-");
                //to_date_check = to_date_check.Replace("/", "-");
                //txtFrom.Text = from_date_check;
                //txtTo.Text = to_date_check;

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                }

                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }

                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);

                            //Get Employee Master Details

                            DataTable dt_Emp_Mst = new DataTable();
                            string MachineID = dt.Rows[j][0].ToString();
                            string Query_Emp_mst = "";
                            Query_Emp_mst = "Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                            Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                            Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                            dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                            //Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName,'2' as Wagestype,
                            //ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode
                            //where ED.CompCode='ESM'
                            //and ED.LocCode='UNIT I' and ED.IsActive='Yes' 
                            //and ED.MachineID='9';

                            if (dt_Emp_Mst.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found in Employee Master. The Employee No is " + MachineID + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            string Department = dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString();
                            string EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();
                            string FirstName = dt.Rows[j][1].ToString();
                            string DeptName = dt.Rows[j][2].ToString();
                            string OTHours = dt.Rows[j][3].ToString();

                       
                         
                            if (FirstName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Department == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            //else if (Home == "")
                            //{
                            //    j = j + 2;
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                            //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            //    ErrFlag = true;
                            //}
                            else if (OTHours == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CL Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            cn.Open();
                            string qry_dpt = "Select DeptName as DepartmentNm from Department_Mst where DeptName = '" + Department + "'";
                            SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                            if (sdr_dpt.HasRows)
                            {

                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_dpt1 = "Select ED.DeptCode as Department from Employee_Mst ED inner Join Department_Mst MSt on Mst.DeptCode=ED.DeptCode where Mst.DeptName = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                            SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                            SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                            if (sdr_dpt1.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                    
                            cn.Open();
                            string qry_empNo = "Select EmpNo from Employee_Mst where EmpNo= '" + EmpNo + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            total = 0;
                            //total = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()) + Convert.ToDecimal(CL) + Convert.ToDecimal(AbsentDays));
                            days = 0;
                            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                            {
                                days = 31;
                            }
                            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                            {
                                days = 30;
                            }
                            else if (ddlMonths.SelectedValue == "February")
                            {
                                int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                                if ((yrs % 4) == 0)
                                {
                                    days = 29;
                                }
                                else
                                {
                                    days = 28;
                                }
                            }
                            //total_check = Convert.ToInt32(total);
                            //if (total_check > Convert.ToInt32(days))
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                            //    ErrFlag = true;
                            //}
                            //total_check = Convert.ToInt32(total);
                            //if (total_check < Convert.ToInt32(txtdays.Text))
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                            //    ErrFlag = true;
                            //}


                            //decimal Att_sum = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()) + Convert.ToDecimal(AbsentDays) + Convert.ToDecimal(CL));
                            //int Att_Sum_Check = 0;
                            //Att_Sum_Check = Convert.ToInt32(Att_sum);
                            //if (Convert.ToInt32(days) < Att_Sum_Check)
                            //{
                            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Attenance Details Properly ...');", true);
                            //    ErrFlag = true;
                            //}
                        }
                        if (!ErrFlag)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                // Employee Master Details
                                DataTable dt_Emp_Mst = new DataTable();
                                DataTable dt_Old = new DataTable();
                                string MachineID = dt.Rows[i][0].ToString();
                                string Query_Emp_mst = "";
                                Query_Emp_mst = "Select ED.Wages, ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm,(ED.DeptCode) as DeptCode, (Wages) as Wagestype,";
                                Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID,(BaseSalary) as BaseSalary,(VPF) as VPF from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                                Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                                dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                                SqlConnection cn = new SqlConnection(constr);

                                if (dt_Emp_Mst.Rows[0]["EmpNo"].ToString() == "3289")
                                {
                                    string StopProcess = "";
                                }

                                if ((dt_Emp_Mst.Rows[0]["Wagestype"].ToString() == "STAFF") || (dt_Emp_Mst.Rows[0]["Wagestype"].ToString() == "FITTER & ELECTRICIANS") || (dt_Emp_Mst.Rows[0]["Wagestype"].ToString() == "DRIVERS") || (dt_Emp_Mst.Rows[0]["Wagestype"].ToString() == "SECURITY"))
                                {
                                    PerDay = ((Convert.ToDecimal(dt_Emp_Mst.Rows[0]["BaseSalary"].ToString()) + Convert.ToDecimal(dt_Emp_Mst.Rows[0]["VPF"].ToString())) / Convert.ToDecimal(26)).ToString();
                                    PerDay = (Math.Round(Convert.ToDecimal(PerDay), 0, MidpointRounding.AwayFromZero)).ToString();
                                    PerHour = (Convert.ToDecimal(PerDay) / Convert.ToDecimal(8)).ToString();
                                    //PerHour = (Math.Round(Convert.ToDecimal(PerHour), 0, MidpointRounding.AwayFromZero)).ToString();
                                }

                                else
                                {
                                    PerDay = (Convert.ToDecimal(dt_Emp_Mst.Rows[0]["BaseSalary"].ToString())).ToString();
                                    PerDay = (Math.Round(Convert.ToDecimal(PerDay), 0, MidpointRounding.AwayFromZero)).ToString();
                                    PerHour = (Convert.ToDecimal(PerDay) / Convert.ToDecimal(8)).ToString();
                                    //PerHour = (Math.Round(Convert.ToDecimal(PerHour), 0, MidpointRounding.AwayFromZero)).ToString();
                                }

                                string CalcOT = dt.Rows[i][3].ToString();

                                TotalPay = (Convert.ToDecimal(PerHour) * Convert.ToDecimal(dt.Rows[i][3].ToString())).ToString();
                                TotalPay = (Math.Round(Convert.ToDecimal(TotalPay), 0, MidpointRounding.AwayFromZero)).ToString();

                                //Get Old Data
                                Query = "";
                                Query = "select * from [" + SessionEpay + "]..OverTime where EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and convert(datetime,FromDate,103)=convert(datetime,'" + txtOtDate.Text + "',103)";
                                dt_Old = objdata.RptEmployeeMultipleDetails(Query);
                                if (dt_Old.Rows.Count > 0)
                                {
                                    Query = "";
                                    Query = "Delete from [" + SessionEpay + "]..OverTime where EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and convert(datetime,FromDate,103)=convert(datetime,'" + txtOtDate.Text + "',103)";
                                    objdata.RptEmployeeMultipleDetails(Query);
                                }
                                Query = "";
                                Query = Query + "insert into [" + SessionEpay + "]..OverTime(EmpNo,EmpName,Department,Daysalary,HrSalary,NoHrs,Month,ChkManual,netAmount,Financialyr,Ccode,Lcode,TransDate,FromDate,ToDate) Values(";
                                Query = Query + "'" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["EmpName"].ToString() + "','" + dt_Emp_Mst.Rows[0]["DeptCode"].ToString() + "',";
                                Query = Query + "'" + PerDay + "','" + PerHour + "','" + dt.Rows[i][3].ToString() + "','" + ddlMonths.Text + "','1','" + TotalPay + "','" + ddlfinance.SelectedValue + "',";
                                Query = Query + "'" + SessionCcode + "','" + SessionLcode + "',convert(datetime,'" + txtOtDate.Text + "',103),convert(datetime,'" + txtOtDate.Text + "',103),convert(datetime,'" + txtOtDate.Text + "',103))";
                                objdata.RptEmployeeMultipleDetails(Query);

                                //if (dt.Rows[i][3].ToString() == "0")
                                //{
                                //    //Skip Zero OT Hours
                                //}
                                //else
                                //{
                                //    TotalPay = (Convert.ToDecimal(PerHour) * Convert.ToDecimal(dt.Rows[i][3].ToString())).ToString();
                                //    TotalPay = (Math.Round(Convert.ToDecimal(TotalPay), 0, MidpointRounding.AwayFromZero)).ToString();

                                    
                                    
                                //    //Get Old Data
                                //    Query = "";
                                //    Query = "select * from [" + SessionEpay + "]..OverTime where EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and convert(datetime,FromDate,103)=convert(datetime,'" + txtOtDate.Text + "',103)";
                                //    dt_Old = objdata.RptEmployeeMultipleDetails(Query);
                                //    if (dt_Old.Rows.Count > 0)
                                //    {
                                //        Query = "";
                                //        Query = "Delete from [" + SessionEpay + "]..OverTime where EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and convert(datetime,FromDate,103)=convert(datetime,'" + txtOtDate.Text + "',103)";
                                //        objdata.RptEmployeeMultipleDetails(Query);
                                //    }
                                //    Query = "";
                                //    Query = Query + "insert into [" + SessionEpay + "]..OverTime(EmpNo,EmpName,Department,Daysalary,HrSalary,NoHrs,Month,ChkManual,netAmount,Financialyr,Ccode,Lcode,TransDate,FromDate,ToDate) Values(";
                                //    Query = Query + "'" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["EmpName"].ToString() + "','" + dt_Emp_Mst.Rows[0]["DeptCode"].ToString() + "',";
                                //    Query = Query + "'" + PerDay + "','" + PerHour + "','" + dt.Rows[i][3].ToString() + "','" + ddlMonths.Text + "','1','" + TotalPay + "','" + ddlfinance.SelectedValue + "',";
                                //    Query = Query + "'" + SessionCcode + "','" + SessionLcode + "',convert(datetime,'" + txtOtDate.Text + "',103),convert(datetime,'" + txtOtDate.Text + "',103),convert(datetime,'" + txtOtDate.Text + "',103))";
                                //    objdata.RptEmployeeMultipleDetails(Query);
                                //}

                                

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                sSourceConnection.Close();
                            }

                            //System.Windows.Forms.MessageBox.Show("Uploaded SuccessFully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                        }
                        if (ErrFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                            //System.Windows.Forms.MessageBox.Show("Your File Not Uploaded", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";

        if (ddlCategory.SelectedValue == "1")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            string SSQL;

            DataTable dt_OT = new DataTable();

            string OT_Above_Eight = "0";
            string OT_Above_Four = "0";
            string OT_Below_Four = "0";
            string OT_Total_Hours = "0";
            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlCategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = ddlEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();

                Query = "";
                Query = Query + "select EmpNo,(FirstName) as EmpName,MachineID,(DeptName) as DepartmentNm,Wages,'' as OTHours,'' as Days from Employee_Mst ";
                Query = Query + "where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  and IsActive='Yes' and OTEligible='1'";
                Query = Query + "and Wages='" + ddlEmployeeType.SelectedItem.Text.ToString() + "'";
                Query = Query + " Order by ExistingCode Asc";
                dt = objdata.RptEmployeeMultipleDetails(Query);


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //OT Hours Above working 8 hours 

                    if ((dt.Rows[i]["Wages"].ToString() == "STAFF") || (dt.Rows[i]["Wages"].ToString() == "FITTER & ELECTRICIANS"))
                    {

                        if (dt.Rows[i]["DepartmentNm"].ToString() == "STAFF 2")
                        {
                            // Aboue 12 Hours
                            SSQL = "";
                            SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(8 as decimal(18,1)))), '0') as OT_8Hours ";
                            SSQL = SSQL + " from LogTime_Days ";
                            SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                            //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                            SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 12";
                            dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }
                        }
                        else
                        {
                            // Aboue 9 Hours
                            SSQL = "";
                            SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(9 as decimal(18,1)))), '0') as OT_8Hours ";
                            SSQL = SSQL + " from LogTime_Days ";
                            SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                            //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                            SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 9";
                            dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }
                        }
                       
                        


                        // Aboue 4 Hours and Below 8 Hours
                        SSQL = "";
                        SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                        // Below 4
                        SSQL = "";
                        SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        //if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }


                        if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                    }
                    else if ((dt.Rows[i]["Wages"].ToString() == "SECURITY") || (dt.Rows[i]["Wages"].ToString() == "DRIVERS"))
                    {
                        // Aboue 12 Hours
                        SSQL = "";
                        SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(12 as decimal(18,1)))), '0') as OT_8Hours ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 13";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }


                        // Aboue 4 Hours and Below 8 Hours
                        SSQL = "";
                        SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(6 as decimal(18,1)))), '0') as OT_8Hours ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 6 and Total_Hrs < 12";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                        // Below 4
                        SSQL = "";
                        SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 6 ";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        //if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }


                        if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                    }
                    else
                    {
                        // Aboue 8 Hours
                        SSQL = "";
                        SSQL = "Select CASE WHEN (isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast( 8 as decimal(18,1)))), '0')) < 0 then '0' else isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast( 8 as decimal(18,1)))), '0') end as OT_8Hours  ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And Present ='1' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > CASE when  Shift='SHIFT3' then 5 else 8 end";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }


                        // Aboue 4 Hours and Below 8 Hours
                        SSQL = "";
                        SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And (Present ='0.5') and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                        // Below 4
                        SSQL = "";
                        SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                        SSQL = SSQL + " from LogTime_Days ";
                        SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                        //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                        SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                        dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                    }

                    OT_Total_Hours = (Convert.ToDecimal(OT_Above_Eight) + Convert.ToDecimal(OT_Above_Four) + Convert.ToDecimal(OT_Below_Four)).ToString();

                    //Get Present Days
                    SSQL = "";
                    SSQL = "Select isnull(SUM(CAST(Present as decimal(18,1))), '0') as Days ";
                    SSQL = SSQL + " from LogTime_Days ";
                    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(txtOtDate.Text).ToString("dd/MM/yyyy") + "',103)";
                    //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                    SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + dt.Rows[i]["MachineID"].ToString() + "'";
                    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_OT.Rows.Count != 0)
                    {
                        dt.Rows[i]["Days"] = dt_OT.Rows[0]["Days"];
                    }

                    dt.Rows[i]["OTHours"] = OT_Total_Hours;

                }

                gvEmp.DataSource = dt;
                gvEmp.DataBind();

                //foreach (GridViewRow gvsalDed in gvEmp.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsalDed.FindControl("lblEmpNo");
                //    Label lbl_Advance = (Label)gvsalDed.FindControl("lblAdvance");
                //    string qry_ot = "";
                //    DataTable Advance_Mst_DT = new DataTable();
                //    DataTable Advance_Pay_DT = new DataTable();
                //    string Advance_Amt = "0.00";
                //    string Balance_Amt = "0";
                //    string Monthly_Paid_Amt = "0";
                //    string Advance_Paid_Amt = "0";
                //    if (ddlEmployeeType.SelectedItem.Text != "CIVIL")
                //    {
                //        qry_ot = "Select * from [" + SessionEpay + "]..AdvancePayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                //        qry_ot = qry_ot + " And EmpNo='" + lbl_EmpNo.Text.Trim() + "' And Completed='N'";
                //        Advance_Mst_DT = objdata.RptEmployeeMultipleDetails(qry_ot);
                //        if (Advance_Mst_DT.Rows.Count != 0)
                //        {
                //            //get Balance Amt
                //            string constr = ConfigurationManager.AppSettings["ConnectionString"];
                //            SqlConnection cn = new SqlConnection(constr);
                //            cn.Open();
                //            qry_ot = "Select isnull(sum(Amount),0) as Paid_Amt from [" + SessionEpay + "]..Advancerepayment where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                //            SqlCommand cmd_wages = new SqlCommand(qry_ot, cn);
                //            Advance_Paid_Amt = (cmd_wages.ExecuteScalar()).ToString();
                //            Balance_Amt = (Convert.ToDecimal(Advance_Mst_DT.Rows[0]["Amount"].ToString()) - Convert.ToDecimal(Advance_Paid_Amt)).ToString();
                //            Monthly_Paid_Amt = Advance_Mst_DT.Rows[0]["MonthlyDeduction"].ToString();
                //            if (Convert.ToDecimal(Monthly_Paid_Amt.ToString()) >= Convert.ToDecimal(Balance_Amt.ToString()))
                //            {
                //                Advance_Amt = (Math.Round(Convert.ToDecimal(Balance_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                //            }
                //            else
                //            {
                //                Advance_Amt = (Math.Round(Convert.ToDecimal(Monthly_Paid_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                //            }
                //            cn.Close();
                //        }
                //        else
                //        {
                //            Advance_Amt = "0.00";
                //        }


                //    }
                //    else
                //    {
                //        Advance_Amt = "0.00";
                //    }

                //    lbl_Advance.Text = Advance_Amt;



                //}

                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=OTDownload.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);

                }


            }
        }
        catch (Exception)
        {

            throw;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

}
