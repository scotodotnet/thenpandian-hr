﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;

public partial class HostelBreakTimeReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    DataTable AutoDTable = new DataTable();
    DataTable dt_break = new DataTable();
    DataTable dt_Lunch = new DataTable();
    DataTable dt_Dinner = new DataTable();
    
    DataTable dt = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    string SSQL;
    BALDataAccess objdata = new BALDataAccess();
    int intK;
    double Present_WH_Count;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Hostel BreakTime Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
          

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();


            getvalues();

           
        }
    }
    public void getvalues()
    {
        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        int daycount1 = (int)((Date2 - date1).TotalDays);
        int daysAdded1 = 0;


        //while (daycount >= 0)
        //{
        //    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
        //    //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
        //    AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

        //    daycount -= 1;
        //    daysAdded += 1;
        //}
        //AutoDTable.Columns.Add("Total Days");


        SSQL = "";
        SSQL = "select MachineID,isnull(FirstName,'') as FirstName,ExistingCode,DeptName";
        SSQL = SSQL + "  from Employee_Mst ";

        SSQL = SSQL + " Where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "' and IsActive='Yes' and Wages='HOSTEL' ";



        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }

        SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName order by ExistingCode";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        intK = 0;

        Present_WH_Count = 0;

        int SNo = 1;


        grid.DataSource = dt;
        grid.DataBind();
        string attachment = "attachment;filename=HostelBreakList.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table border='1'>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td align='center' colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">Hostel Break Time Report</a>");
        Response.Write("</td>");

        Response.Write("</tr Font-Bold='true' align='center'>");

        Response.Write("<tr>");


        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> SNo </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> DeptName </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> MachineID </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> ExistingCode </a>");
        Response.Write("</td>");
        Response.Write("<td rowspan='2'>");
        Response.Write("<a style=\"font-weight:bold\"> FirstName </a>");
        Response.Write("</td>");

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            Response.Write("<td colspan=3>");
            Response.Write("<a style=\"font-weight:bold\"> " + AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString())) + " </a>");
            Response.Write("</td>");

            daycount -= 1;
            daysAdded += 1;
        }
        Response.Write("<td colspan='3'>");
        Response.Write("<a style=\"font-weight:bold\"> Total Days</a>");
        Response.Write("</td>");


        Response.Write("</tr>");
        Response.Write("<tr>");

        while (daycount1 >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded1).ToShortDateString());
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> Break </a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> Lunch </a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> Dinner </a>");
            Response.Write("</td>");

            daycount1 -= 1;
            daysAdded1 += 1;
        }

        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\"> Break </a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\"> Lunch </a>");
        Response.Write("</td>");

        Response.Write("<td>");
        Response.Write("<a style=\"font-weight:bold\"> Dinner </a>");
        Response.Write("</td>");

        Response.Write("</tr>");


        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();

            AutoDTable.Rows[i]["SNo"] = SNo;
            AutoDTable.Rows[i]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            AutoDTable.Rows[i]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[i]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[i]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
            string InMachine_IP = "";

            InMachine_IP = UTF8Encryption(AutoDTable.Rows[i]["MachineID"].ToString());

            if (AutoDTable.Rows[i]["MachineID"] == "2")
            {
                string s = "1";
            }
            Response.Write("<tr align='left'>");

            Response.Write("<td>");
            Response.Write("<a>" + Convert.ToInt16(i + 1) + "</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a>" + AutoDTable.Rows[i]["DeptName"].ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a>" + AutoDTable.Rows[i]["MachineID"].ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a>" + AutoDTable.Rows[i]["ExistingCode"].ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a>" + AutoDTable.Rows[i]["FirstName"].ToString() + "</a>");
            Response.Write("</td>");

            decimal count1 = 0;
            decimal count2 = 0;
            decimal count3 = 0;

            for (int j = 0; j < daysAdded; j++)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                Date_Value_Str = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
                //            dt_break = new DataTable();
                //DataTable dt_Lunch = new DataTable();
                //DataTable dt_Dinner

                SSQL = "Select * from LogTimeHostel_Lunch where MachineID='" + InMachine_IP + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Status='BreakFast' ";
                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(j).ToString("yyyy/MM/dd") + " " + "07:30" + "'";
                SSQL = SSQL + " And TimeIN <='" + date1.AddDays(j).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeIN Asc";
                dt_break = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_break.Rows.Count != 0)
                {
                    Response.Write("<td>");
                    Response.Write("<span style=color:green><a> X </a></span>");
                    Response.Write("</td>");
                    count1 = count1 + 1;
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("<span style=color:red><a> A </a></span>");
                    Response.Write("</td>");
                }

                SSQL = "Select * from LogTimeHostel_Lunch where MachineID='" + InMachine_IP + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Status='Lunch' ";
                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(j).ToString("yyyy/MM/dd") + " " + "11:30" + "'";
                SSQL = SSQL + " And TimeIN <='" + date1.AddDays(j).ToString("yyyy/MM/dd") + " " + "15:00" + "' Order by TimeIN Asc";
                dt_Lunch = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Lunch.Rows.Count != 0)
                {
                    Response.Write("<td>");
                    Response.Write("<span style=color:green><a> X </a></span>");
                    Response.Write("</td>");
                    count2 = count2 + 1;
                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("<span style=color:red><a> A </a></span>");
                    Response.Write("</td>");
                }


                SSQL = "Select * from LogTimeHostel_Lunch where MachineID='" + InMachine_IP + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Status='Dinner' ";
                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(j).ToString("yyyy/MM/dd") + " " + "18:30" + "'";
                SSQL = SSQL + " And TimeIN <='" + date1.AddDays(j).ToString("yyyy/MM/dd") + " " + "23:00" + "' Order by TimeIN Asc";
                dt_Dinner = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Dinner.Rows.Count != 0)
                {
                    Response.Write("<td>");
                    Response.Write("<span style=color:green><a> X </a></span>");
                    Response.Write("</td>");
                    count3 = count3 + 1;

                }
                else
                {
                    Response.Write("<td>");
                    Response.Write("<span style=color:red><a> A </a></span>");
                    Response.Write("</td>");
                }
                //count = count + 1;
            }
            Response.Write("<td>");
            Response.Write("<span style=color:red><a>" + count1 + " </a></span>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<span style=color:red><a>" + count2 + " </a></span>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<span style=color:red><a>" + count3 + " </a></span>");
            Response.Write("</td>");


            intK = intK + 1;

            SNo = SNo + 1;

            count1 = 0;


            Response.Write("</tr>");
          
        }
        Response.Write("</table>");

        Response.End();
        Response.Clear();
    }

    public static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
   
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

}
