﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class OnDutyProcess : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string TempDate;
    static int d = 0;
    string SessionMonth;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: OnDuty Process";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            load_TokenNo();
            if (Session["TransID"] != null)
            {
                txtExistCode.Text = Session["TransID"].ToString();
                SessionMonth = Session["Month"].ToString();
                txtExistCode.Enabled = false;
                btnSearch_Click(sender, e);
            }
        }
    }
    public void load_TokenNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlTokenNo.Items.Clear();
        query = "Select  cast(MachineID as varchar(10)) as MachineID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  And IsActive='Yes' ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["MachineID"] = "-Select-";
        dr["MachineID"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "MachineID";
        ddlTokenNo.DataValueField = "MachineID";
        ddlTokenNo.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();

        SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and D_Status='1'";
        SSQL = SSQL + " and ExistingCode='" + txtExistCode.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtExistCode.Text = DT.Rows[0]["ExistingCode"].ToString();
            ddlTokenNo.SelectedValue = DT.Rows[0]["TokenNo"].ToString();
            txtEmpName.Text = DT.Rows[0]["EmpName"].ToString();
            ddlMonth.SelectedValue = DT.Rows[0]["Months"].ToString().Trim();
            txtFromDate.Text = DT.Rows[0]["ONDutyFromDate"].ToString();
            txtToDate.Text  = DT.Rows[0]["ONDutyToDate"].ToString();
            txtDays.Text = DT.Rows[0]["Days"].ToString();
            txtFromArea.Text = DT.Rows[0]["FromArea"].ToString();
            txtToArea.Text = DT.Rows[0]["ToArea"].ToString();
            txtReason.Text = DT.Rows[0]["Reason"].ToString();
            txtVechileno.Text = DT.Rows[0]["VechileNo"].ToString();
            txtCharge.Text= DT.Rows[0]["TravelCharge"].ToString();
            txtReturnDate.Text = DT.Rows[0]["ONDutyReturnDate"].ToString();
            btnSave.Text = "Update";
        }

    }
    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtExistCode.Text = DT.Rows[0]["ExistingCode"].ToString();
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("OnDutyProcessMain.aspx");
    }
    private void Clear_All_Field()
    {
        txtExistCode.Text = "";
        txtEmpName.Text = "";
        ddlTokenNo.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtDays.Text = "";
        
        txtFromArea.Text = ""; txtToArea.Text = "";
        txtReason.Text = "";
        txtVechileno.Text = "";
        txtCharge.Text = "";
        txtReturnDate.Text = "";

        Session.Remove("TransID");
        
        btnSave.Text = "Save";


    }
    protected void txtFromDate_TextChanged(object sender, EventArgs e)
    {
        DayDiff();

        TempDate = Convert.ToDateTime(txtFromDate.Text).AddMonths(0).ToShortDateString();
        string MonthName = TempDate;

        DateTime mtn;
        mtn = Convert.ToDateTime(TempDate);
        d = mtn.Month;
        int mons = mtn.Month;
        int yrs1 = mtn.Month;
        #region MonthName
        string DB_Mont = "";
        switch (mons)
        {
            case 1:
                DB_Mont = "January";
                break;

            case 2:
                DB_Mont = "February";
                break;
            case 3:
                DB_Mont = "March";
                break;
            case 4:
                DB_Mont = "April";
                break;
            case 5:
                DB_Mont = "May";
                break;
            case 6:
                DB_Mont = "June";
                break;
            case 7:
                DB_Mont = "July";
                break;
            case 8:
                DB_Mont = "August";
                break;
            case 9:
                DB_Mont = "September";
                break;
            case 10:
                DB_Mont = "October";
                break;
            case 11:
                DB_Mont = "November";
                break;
            case 12:
                DB_Mont = "December";
                break;
            default:
                break;
        }
        #endregion

        if (ddlMonth.SelectedValue.ToString().Trim() != DB_Mont)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check Month & Date Properly.');", true);
            txtFromDate.Text = "";
            //ErrFlag = true;
        }

    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        DayDiff();

        TempDate = Convert.ToDateTime(txtToDate.Text).AddMonths(0).ToShortDateString();
        string MonthName = TempDate;

        DateTime mtn;
        mtn = Convert.ToDateTime(TempDate);
        d = mtn.Month;
        int mons = mtn.Month;
        int yrs1 = mtn.Month;
        #region MonthName
        string DB_Mont = "";
        switch (mons)
        {
            case 1:
                DB_Mont = "January";
                break;

            case 2:
                DB_Mont = "February";
                break;
            case 3:
                DB_Mont = "March";
                break;
            case 4:
                DB_Mont = "April";
                break;
            case 5:
                DB_Mont = "May";
                break;
            case 6:
                DB_Mont = "June";
                break;
            case 7:
                DB_Mont = "July";
                break;
            case 8:
                DB_Mont = "August";
                break;
            case 9:
                DB_Mont = "September";
                break;
            case 10:
                DB_Mont = "October";
                break;
            case 11:
                DB_Mont = "November";
                break;
            case 12:
                DB_Mont = "December";
                break;
            default:
                break;
        }
        #endregion

        if (ddlMonth.SelectedValue.ToString().Trim() != DB_Mont)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check Month & Date Properly.');", true);
            txtToDate.Text = "";
            //ErrFlag = true;
        }
    }
    public void Load_Check_Days_Properly(string Dates)
    {
       
    }
    private void DayDiff()
    {

        bool ErrFlag = false;
        if ((ddlMonth.SelectedValue == "0") || (ddlMonth.SelectedValue == "-Select-"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
            ErrFlag = true;
            txtFromDate.Text = "";
            txtToDate.Text = "";
        }
        if ((txtFromDate.Text != "") || (txtToDate.Text == ""))
        {
            Load_Check_Days_Properly(txtFromDate.Text);
            txtDays.Text = "0";
        }
        else if ((txtFromDate.Text == "") || (txtToDate.Text != ""))
        {
            Load_Check_Days_Properly(txtToDate.Text);
            txtDays.Text = "0";
        }

        
        if (txtFromDate.Text != "" && txtToDate.Text != "")
        {
           


            DateTime Date1 = Convert.ToDateTime(txtFromDate.Text);
            DateTime Date2 = Convert.ToDateTime(txtToDate.Text);

            int daycount = (int)((Date2 - Date1).TotalDays);
            int daysAdded = daycount + 1;

            txtDays.Text = daysAdded.ToString();
        }
        else
        {
            txtDays.Text = "0";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the From Date and To Date correctly..!');", true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable dt_Check = new DataTable();
        bool ErrFlag = false;

        //SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " and ExistingCode='" + txtExistCode.Text + "'   and Convert(datetime,ONDutyFromDate,103)>=Convert(datetime,'" + txtFromDate.Text + "',103) and Convert(datetime,ONDutyReturnDate,103)<=Convert(datetime,'" + txtToDate.Text + "',103)  and D_Status='2'";
        //dt_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        // txtFromDate_TextChanged(sender, e);

        //if (dt_Check.Rows.Count > 0)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This Employee Already got OnDudy on this date ..,');", true);
        //    ErrFlag = true;
        //}

        if (!ErrFlag)
        {
            SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and ExistingCode='" + txtExistCode.Text + "' and D_Status='1' ";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                //Delete OLD Record
                SSQL = "Delete from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and ExistingCode='" + txtExistCode.Text + "' and D_Status='1' ";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }


            //Insert Commision Voucher
            SSQL = "Insert Into OnDuty_Mst(CompCode,LocCode,ExistingCode,TokenNo,EmpName,ONDutyFromDate,ONDutyToDate,Days,FromArea,ToArea,Reason,VechileNo,TravelCharge,ONDutyReturnDate,D_Status,Months)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtExistCode.Text + "','" + ddlTokenNo.SelectedItem.Text + "','" + txtEmpName.Text + "',";
            SSQL = SSQL + "'" + txtFromDate.Text + "','" + txtToDate.Text + "','" + txtDays.Text + "','" + txtFromArea.Text + "','" + txtToArea.Text + "',";
            SSQL = SSQL + "'" + txtReason.Text + "','" + txtVechileno.Text + "','" + txtCharge.Text + "','" + txtToDate.Text + "','1','" + ddlMonth.SelectedValue + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            Session.Remove("TransID");
            Response.Redirect("OnDutyProcessMain.aspx");


            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ON Duty Deatils Saved Successfully..');", true);
            Clear_All_Field();
        }
       
    }
    protected void btnApproval_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        
        bool ErrFlag = false;

        SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and ExistingCode='" + txtExistCode.Text + "' and D_Status='1' ";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('First you want to save this Employee..,');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = SSQL + "Update OnDuty_Mst set D_Status='2' where ExistingCode='" + txtExistCode.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Approval Successfully..');", true);
            Session.Remove("TransID");
            Response.Redirect("OnDutyProcessMain.aspx");
        }
    }
}
