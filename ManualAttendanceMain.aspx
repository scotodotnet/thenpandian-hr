﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ManualAttendanceMain.aspx.cs" Inherits="ManualAttendanceMain" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>

<script>
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
              
            }
        });
    };
</script>

<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Manual Attendance</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Manual Attendance </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Manual Attendance</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       
                        <!-- begin row -->  
                        <div class="row">
                         
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnAdd" Text="ADD" class="btn btn-success" 
                                         onclick="btnAdd_Click" />
								
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->  
                        
                        
                          <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <th>Emp No</th>
                                                <th>Emp Name</th>
                                                <th>Attn Date</th>
                                                <th>IN</th>
                                                <th>OUT</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("EmpNo")%></td>
                                    <td><%# Eval("EmpName")%></td>
                                    <td><%# Eval("AttnDateStr")%></td>
                                    <td><%# Eval("TimeIn")%></td>
                                    <td><%# Eval("TimeOut")%></td>
                                   
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</asp:Content>

