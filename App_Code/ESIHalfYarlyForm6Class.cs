﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ESIHalfYarlyForm6Class
/// </summary>
public class ESIHalfYarlyForm6Class
{
    private string _EmpNo;
    private string _ESINumber;
    private string _ESIAmount;
    private string _ESIOffice;
    private string _RateofFirstWages;
    private string _NoofdayswagesPeriod;
    private string _TotalAmtWagesPeriod;
    private string _EmpShareContripution;
    
	public ESIHalfYarlyForm6Class()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string ESINumber
    {
        get { return _ESINumber; }
        set { _ESINumber = value; }
    }
    public string ESIAmount
    {
        get { return _ESIAmount; }
        set { _ESIAmount = value; }
    }
    public string ESIOffice
    {
        get { return _ESIOffice; }
        set { _ESIOffice= value; }
    }
    public string RateofFirstWages
    {
        get { return _RateofFirstWages; }
        set { _RateofFirstWages = value; }
    }
   
    public string NoofdayswagesPeriod
    {
        get { return _NoofdayswagesPeriod; }
        set { _NoofdayswagesPeriod = value; }

    }
    public string TotalAmtWagesPeriod
    {
        get { return _TotalAmtWagesPeriod; }
        set { _TotalAmtWagesPeriod = value; }
    }
    public string EmpShareContribution
    {
        get { return _EmpShareContripution; }
        set { _EmpShareContripution = value; }
    }
}
