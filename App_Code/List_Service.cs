﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;

/// <summary>
/// Summary description for List_Service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]


public class List_Service : System.Web.Services.WebService {

    BALDataAccess objdata = new BALDataAccess();

    public List_Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMedicalCode_Det(string prefix)
    {
        List<string> Supplier_Code = new List<string>();
        string query = "";
        string Comp_Code = "";
        string Loc_Code = "";
        Comp_Code = HttpContext.Current.Session["Ccode"].ToString();
        Loc_Code = HttpContext.Current.Session["Lcode"].ToString();
        DataTable DT = new DataTable();
        query = "Select Distinct MedicalDetails from MedicalDetails where Ccode='" + Comp_Code + "' And Lcode='" + Loc_Code + "'";
        query = query + " And (MedicalDetails like '%" + prefix + "%')";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                Supplier_Code.Add(DT.Rows[i]["MedicalDetails"].ToString());
            }
        }

        return Supplier_Code.ToArray();
    }
}

