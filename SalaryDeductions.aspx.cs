﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

public partial class SalaryDeductions : System.Web.UI.Page
{
    string Nfh_Work_Amt = "0";
    string Get_Leave = "0";
    string Total_Days_Calc = "0";
    string Balance_Days = "0";
    string Mess_Company_Amt = "0";
    string Mess_Emp_Amt = "0";

    DataTable dt_Leave = new DataTable();
    string Age = "";

    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Adolescent_Date_Str = "";
    string Total_Wages = "0";
    string Total_All_Wages = "0";
    string New_Mess_Days = "0";
    string New_Mess_Amt = "0";
    string New_Agent_Days = "0";
    string New_Agent_Amt = "0";

    string MinDays = "0";
    string IncAmt = "0";
    string Query = "";
    string VPF = "0";
    string Cash_Amt = "0";
    string Bank_Amt = "0";
    string Allowance_Amt = "0";
    string Total_Amt = "0";
    string Deduct_Amt = "";

    string OnDuty = "0";
    DataTable dt_OnDuty = new DataTable();

    DateTime mydate;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    string FromBankACno, FromBankName, FromBranch, ProvisionalTax = "0";
    SqlConnection con;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    string Cash;
    static string FDA = "0";
    SalaryClass objSal = new SalaryClass();
    string CCA = "0";

    string Total_Check_Days = "0";
    DataTable dt_CL = new DataTable();
    string CL_Days = "0";
    string MissDays = "0";
    DataTable dt_CL_Temp = new DataTable();
    string C_Total = "0";
    string CL_Name, CL_From, CL_To, CL_Dept, CL_Month, CL_Year;

    string SessionUserType;
    static string EmployeeDays = "0";
    static string Work = "0";
    static string NFh = "0";
    static string lopdays = "0";
    static string WeekOff = "0";
    static string Home = "0";
    static string halfNight = "0";
    static string FullNight = "0";
    static string ThreeSided = "0";
    static string HalfNight_Amt = "0";
    static string FullNight_Amt = "0";
    static string ThreeSided_Amt = "0";
    static string SpinningAmt = "0";
    static string DayIncentive = "0";
    static string cl = "0";
    static string OTDays = "0";
    static string basic = "0";
    static string All1 = "0";
    static string All2 = "0";
    static string All3 = "0";
    static string Al4 = "0";
    static string All5 = "0";
    static string Ded1 = "0";
    static string ded2 = "0";
    static string ded3 = "0";
    static string ded4 = "0";
    static string ded5 = "0";
    static string lop = "0";
    static string LeaveDays = "0";
    static string Advance = "0";
    static string pfAmt = "0";
    static string ESI = "0";
    static string stamp = "0";
    static string Union = "0";
    static string TotalEarnings = "0";
    static string TotalDeductions = "0";
    static string NetPay = "0";
    static string RoundOffNetPay = "0";
    static string Words = "";
    static string PFS = "0.00";
    static string ExistNo = "";
    static string PF_salary;
    string TempDate;
    DateTime TransDate;
    DateTime MyDate;
    static int d = 0;
    static decimal AdvAmt;
    static string ID;
    static bool isUK = false;
    string SMonth;
    string Year;
    int dd;
    string MyMonth;
    static decimal val;
    string WagesType;
    static decimal basicsalary = 0;
    static string Basic_For_SM = "0";
    static string OTHoursNew_Str = "0";
    static string OTHoursAmtNew_Str = "0";
    static string query2 = "0";

    static string Fixed_Work_Days = "0";
    static string WH_Work_Days = "0";
    static string DedOthers1 = "0";
    static string DedOthers2 = "0";
    static string MedicalReiburse = "0", PerIncentive = "0", SalaryAdvanceTrans = "0", IncomTax = "0", MessDeduction = "0", HostelDeduction = "0", Totalpay = "0";
    static string EmployeerPFOne = "0.00";
    static string EmployeerPFTwo = "0.00";
    static string EmployeerESI = "0.00";
    string SessionEpay;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            DateTime currentdate = Convert.ToDateTime(DateTime.Now);
            string Tdate = currentdate.ToString("dd/MM/yyyy");
            txtTransDate.Text = Tdate;
            if (SessionAdmin == "2")
            {
                //IF_Civil_Inc.Visible = false;
                //IF_Token_Checkbox.Visible = false;
                IF_Token_No.Visible = false;
            }

        }
    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    public void Load_TwoDates()
    {
        if (ddlMonth.SelectedValue != "-Select-" && ddlFinYear.SelectedValue != "-Select-" && ddlEmployeeType.SelectedValue != "")
        {
            if (ddlEmployeeType.SelectedValue != "2" && ddlEmployeeType.SelectedValue != "5" && ddlEmployeeType.SelectedValue != "10")
            {
                decimal Month_Total_days = 0;
                string Month_Last = "";
                string Year_Last = "0";
                string Temp_Years = "";
                string[] Years;
                string FromDate = "";
                string ToDate = "";

                Temp_Years = ddlFinYear.SelectedItem.Text.ToString();
                Years = Temp_Years.Split('-');
                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
                {
                    Month_Total_days = 31;
                }
                else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
                {
                    Month_Total_days = 30;
                }

                else if (ddlMonth.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(Years[0]) + 1);
                    if ((yrs % 4) == 0)
                    {
                        Month_Total_days = 29;
                    }
                    else
                    {
                        Month_Total_days = 28;
                    }
                }
                switch (ddlMonth.SelectedItem.Text)
                {
                    case "January":
                        Month_Last = "01";
                        break;
                    case "February":
                        Month_Last = "02";
                        break;
                    case "March":
                        Month_Last = "03";
                        break;
                    case "April":
                        Month_Last = "04";
                        break;
                    case "May":
                        Month_Last = "05";
                        break;
                    case "June":
                        Month_Last = "06";
                        break;
                    case "July":
                        Month_Last = "07";
                        break;
                    case "August":
                        Month_Last = "08";
                        break;
                    case "September":
                        Month_Last = "09";
                        break;
                    case "October":
                        Month_Last = "10";
                        break;
                    case "November":
                        Month_Last = "11";
                        break;
                    case "December":
                        Month_Last = "12";
                        break;
                    default:
                        break;
                }

                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "February") || (ddlMonth.SelectedValue == "March"))
                {
                    Year_Last = Years[1];
                }
                else
                {
                    Year_Last = Years[0];
                }
                FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
                ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

                txtFromDate.Text = FromDate.ToString();
                txtToDate.Text = ToDate.ToString();

            }
            else
            {
                txtToDate.Text = "";
                txtFromDate.Text = "";
            }

        }
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEmployeeType.SelectedValue.ToString().ToUpper() == "CIVIL".ToString().ToUpper())
        {
            //PanelCivilIncenDate.Visible = true;
        }
        else
        {
            PanelCivilIncenDate.Visible = false;
            Load_TwoDates();
            Load_MachineID();
        }

    }

    private void Load_MachineID()
    {
        string SSQL = "";
        SSQL = "select MachineID,ExistingCode from Employee_Mst Where Compcode='" + SessionCcode + "' and Loccode='" + SessionLcode + "' and Wages='" + ddlEmployeeType.SelectedValue + "'";
        ddlEmpno.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmpno.DataTextField = "MachineID";
        ddlEmpno.DataValueField = "ExistingCode";
        ddlEmpno.DataBind();
        ddlEmpno.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";

        if (ddlCategory.SelectedValue == "1")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }

    protected void ddlEmpno_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        txtTokenNo.Text = ddlEmpno.SelectedValue;
        SSQL = "";
        SSQL = "select (FirstName +' '+LastName) as EmpName from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + ddlEmpno.SelectedItem.Text + "'";
        SqlConnection cn = new SqlConnection(constr);
        cn.Open();
        SqlCommand cmd = new SqlCommand(SSQL, cn);
        txtEmpName.Text = cmd.ExecuteScalar().ToString();
        cn.Close();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlFinYear.ClearSelection();
        ddlMonth.ClearSelection();
        ddlCategory.ClearSelection();
        ddlEmpno.ClearSelection();
        txtTokenNo.Text = "";
        txtEmpName.Text = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
}