﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="SalaryDeductions.aspx.cs" Inherits="SalaryDeductions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Salary Process</a></li>
            <li class="active">Salary Deduction</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Salary Deduction</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="SalPay" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Salary Deduction</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Fin. Year</label>
                                                <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged" Enabled="false">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Month</label>
                                                <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="January">January</asp:ListItem>
                                                    <asp:ListItem Value="February">February</asp:ListItem>
                                                    <asp:ListItem Value="March">March</asp:ListItem>
                                                    <asp:ListItem Value="April">April</asp:ListItem>
                                                    <asp:ListItem Value="May">May</asp:ListItem>
                                                    <asp:ListItem Value="June">June</asp:ListItem>
                                                    <asp:ListItem Value="July">July</asp:ListItem>
                                                    <asp:ListItem Value="August">August</asp:ListItem>
                                                    <asp:ListItem Value="September">September</asp:ListItem>
                                                    <asp:ListItem Value="October">October</asp:ListItem>
                                                    <asp:ListItem Value="November">November</asp:ListItem>
                                                    <asp:ListItem Value="December">December</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">Staff</asp:ListItem>
                                                    <asp:ListItem Value="2">Labour</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlEmployeeType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Machine ID</label>
                                                <asp:DropDownList runat="server" ID="ddlEmpno" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlEmpno_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->

                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">

                                        <!-- begin col-3 -->
                                        <div class="col-md-2" runat="server" id="IF_Token_No">
                                            <div class="form-group">
                                                <label>TokenNo</label>
                                                <asp:TextBox runat="server" ID="txtTokenNo" Font-Bold="true" class="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Employee Name</label>
                                                <asp:TextBox runat="server" ID="txtEmpName" Font-Bold="true" class="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Transfer Date</label>
                                                <asp:TextBox runat="server" ID="txtTransDate" Font-Bold="true" class="form-control datepicker" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <br />
                                    <div class="panel-footer">
                                    </div>
                                    <!-- end row -->
                                     <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Allowance 3</label>
                                                <asp:TextBox runat="server" ID="txtAll3" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Allowance 4</label>
                                                <asp:TextBox runat="server" ID="txtAll4" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Allowance 5</label>
                                                <asp:TextBox runat="server" ID="txtAll5" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Deduction 3</label>
                                                <asp:TextBox runat="server" ID="txtDedu3" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Deduction 4</label>
                                                <asp:TextBox runat="server" ID="txtDedu4" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Deduction 5</label>
                                                <asp:TextBox runat="server" ID="txtDedu5" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Advance</label>
                                                <asp:TextBox runat="server" ID="txtAdvance" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Allowance 3</label>
                                                <asp:TextBox runat="server" ID="TextBox7" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-md-1" align="center">
                                            <div class="form-group">
                                                 <label>Allowance 3</label>
                                                <asp:TextBox runat="server" ID="TextBox8" Font-Bold="true" class="form-control" Text="0.00" ></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-12" align="center">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    OnClick="btnSave_Click" OnClientClick="ProgressBarShow();" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear"
                                                    class="btn btn-danger" OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                    <div class="row">
                                        <asp:Panel ID="PanelCivilIncenDate" runat="server" Visible="false">
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Civil From Date</label>
                                                    <asp:TextBox runat="server" ID="txtIstWeekDate" class="form-control datepicker"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Civil To Date</label>
                                                    <asp:TextBox runat="server" ID="txtLastWeekDate" class="form-control datepicker"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </asp:Panel>
                                    </div>
                                </div>

                                <div id="Download_loader" style="display: none" />
                            </div>
                            </div>

                        </div>
                        <!-- end panel -->
                    </div>

                </ContentTemplate>
               
            </asp:UpdatePanel>

            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>

</asp:Content>

