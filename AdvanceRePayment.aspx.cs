﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Net;

public partial class AdvanceRePayment : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    AdvanceAmount objsal = new AdvanceAmount();
    //bool searchFlag = false;
    string Datetime_ser;
    DateTime mydate;
    //string Mont;
    //int Monthid;
    string SessionAdmin;
    static string id = "";
    static string inc_Month;
    static string dec_month;
    string Query;
    string MyMonth;
    string IncrementMonths;
    string decrementMonths;
    string SessionCcode;
    string SessionLcode;
    string SessionPayroll;
    int currentYear = 0;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    protected void Page_Load(object sender, EventArgs e)
    {
        
       
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        currentYear = Utility.GetFinancialYear;
        Load_DB();
        if (!IsPostBack)
        {
            SettingServerDate();
            Load_WagesCode();
            Load_Repay_Type();
            //ddlWagesType_SelectedIndexChanged(sender, e);

        }
      
    }
    public void Load_Repay_Type()
    {
        if (rbtnType.SelectedValue == "1")
        {
            NormalPanel.Visible = true;
            ddlWagesType.Enabled = true;
            Load_Wages();
            
        }
        else
        {
            NormalPanel.Visible = true;
            ddlWagesType.Enabled = false;
            Load_Wages();
            Load_WagesCode();
            
        }
    }

    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from ["+Session["Rights"]+"]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Load_WagesCode()
    {
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }
    public void Load_Wages()
    {
        if (rbtnType.SelectedValue == "1")
        {
            DataTable dtdsupp = new DataTable();
            ddlExistingCode.Items.Clear();
            Query = "Select ExistingCode from Employee_Mst where Wages='" + ddlWagesType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And IsActive='Yes'";
            dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
            ddlExistingCode.DataSource = dtdsupp;
            DataRow dr = dtdsupp.NewRow();
            dr["ExistingCode"] = "-Select-";
            dr["ExistingCode"] = "-Select-";
            dtdsupp.Rows.InsertAt(dr, 0);
            ddlExistingCode.DataTextField = "ExistingCode";
            ddlExistingCode.DataValueField = "ExistingCode";
            ddlExistingCode.DataBind();
        }
        else
        {
            DataTable dtdsupp = new DataTable();
            ddlExistingCode.Items.Clear();
            Query = "Select ExistingCode from Employee_Mst where DeptName='AGENT' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And IsActive='Yes'";
            dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
            ddlExistingCode.DataSource = dtdsupp;
            DataRow dr = dtdsupp.NewRow();
            dr["ExistingCode"] = "-Select-";
            dr["ExistingCode"] = "-Select-";
            dtdsupp.Rows.InsertAt(dr, 0);
            ddlExistingCode.DataTextField = "ExistingCode";
            ddlExistingCode.DataValueField = "ExistingCode";
            ddlExistingCode.DataBind();
        }
    }
    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Wages();
       
    }
    protected void ddlExistingCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtTokenNo.Text = "";
        bool ErrFlag = false;
        clear();
        if (ddlExistingCode.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Existing Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
          
          DataTable dt = new DataTable();
            string SSQL = "";

            SSQL = "";
            SSQL = "select * from [" + SessionPayroll + "]..advDetails  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status='Pending' and ExistingCode='" + ddlExistingCode.SelectedItem.Text + "'";
            DataTable dt_check = new
                 DataTable();
            dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_check.Rows.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee number not Found in Advance');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
                return;
            }

            if (!ErrFlag)
            {
                Query = "Select ED.EmpNo,ED.ExistingCode,ED.FirstName ,ED.Designation,Convert(varchar,ED.DOJ,105) as Dateofjoining,";
                Query = Query + " ED.BaseSalary,ED.DeptName  from Employee_Mst ED  ";
                Query = Query + " where ED.ExistingCode ='" + ddlExistingCode.SelectedItem.Text + "' and ED.IsActive='Yes' and ED.Compcode='" + SessionCcode + "' and ED.Loccode='" + SessionLcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(Query);

                if (dt.Rows.Count > 0)
                {
                    txtEmpName.Text = dt.Rows[0]["FirstName"].ToString();
                    txtTokenNo.Text = dt.Rows[0]["EmpNo"].ToString();
                    txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
                }


                SSQL = "Select isnull(SUM(Balance),'0') from [" + SessionPayroll + "]..advdetails  where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Status='Pending' and ExistingCode='" + ddlExistingCode.SelectedItem.Text + "'";
                SSQL = SSQL + " group by ExistingCode";
                SqlConnection cn = new SqlConnection(constr);
                SqlCommand cmd = new SqlCommand(SSQL, cn);
                cn.Open();
                txtBalanceAmt.Text = cmd.ExecuteScalar().ToString();
                cn.Close();
                // clear();
            }

            //Query= "Select ED.EmpNo,ED.ExistingCode,ED.FirstName ,ED.Designation,Convert(varchar,ED.DOJ,105) as Dateofjoining,";
            //Query = Query + " ED.BaseSalary,ED.DeptName  from Employee_Mst ED  ";
            //Query =Query + " where ED.ExistingCode ='"+ddlExistingCode.SelectedItem.Text +"' and ED.IsActive='Yes' and ED.Compcode='"+SessionCcode +"' and ED.Loccode='"+SessionLcode +"' ";
            //dt = objdata.RptEmployeeMultipleDetails(Query);

            //  if (dt.Rows.Count > 0)
            //  {
            //      txtEmpName.Text = dt.Rows[0]["FirstName"].ToString();
            //      txtTokenNo.Text = dt.Rows[0]["EmpNo"].ToString();
            //      txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
            //      DataTable dtrepay = new DataTable();
            //      Query = " Select EmpNo,ExistNo,BalanceAmount,MonthlyDeduction,ID,IncreaseMonth,ReductionMonth";
            //      Query = Query + " from [" + SessionPayroll + "].. AdvancePayment where ExistNo ='"+ddlExistingCode.SelectedItem.Text+"' and";
            //      Query = Query + " Completed='N' and Ccode='"+SessionCcode +"' and Lcode='"+SessionLcode +"' ";
            //      dtrepay = objdata.RptEmployeeMultipleDetails(Query);
            //      {
            //          if (dtrepay.Rows.Count > 0)
            //          {
            //              txtBalanceAmt.Text = dtrepay.Rows[0]["BalanceAmount"].ToString();
            //              id = dtrepay.Rows[0]["ID"].ToString();
            //              inc_Month = dtrepay.Rows[0]["IncreaseMonth"].ToString();
            //              dec_month = dtrepay.Rows[0]["ReductionMonth"].ToString();
            //              if (Convert.ToDecimal(txtBalanceAmt.Text) < Convert.ToDecimal(dtrepay.Rows[0]["MonthlyDeduction"].ToString()))
            //              {
            //                  txtAmount.Text = txtBalanceAmt.Text;
            //              }
            //              else
            //              {
            //                  txtAmount.Text = dtrepay.Rows[0]["MonthlyDeduction"].ToString();
            //              }
            //          }
            //          else
            //          {
            //              txtTokenNo.Text = "";
            //              txtDesignation.Text = "";
            //              txtAmount.Text = "";
            //              txtEmpName.Text = "";
            //              id = "";
            //              //lblAmt.Text = "";
            //              ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found....!');", true);
            //              //System.Windows.Forms.MessageBox.Show("Employee Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //          }
            //      }
            //  }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            if (ddlExistingCode.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee No....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee No....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
           
            else if (txtAmount.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (Convert.ToDecimal(txtBalanceAmt.Text) < Convert.ToDecimal(txtAmount.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (Convert.ToDecimal(txtAmount.Text) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                //else if (dec_month == "1")
                //{
                //    if ((Convert.ToDecimal(txtAmount.Text) != Convert.ToDecimal(txtBalanceAmt.Text)))
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Pay the full Amount....!');", true);
                //        //System.Windows.Forms.MessageBox.Show("Pay the full Amount....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                //        ErrFlag = true;
                //    }
                //}
                if (!ErrFlag)
                {
                    if (Convert.ToDecimal(txtAmount.Text) > Convert.ToDecimal(txtBalanceAmt.Text))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Amount is should be Lesser than Balance Amount');", true);
                        return;
                    }
                    else
                    {
                        string IPAddress = "";
                        string HostName = "";

                        HostName = Dns.GetHostName();
                        IPAddress = Dns.GetHostByName(HostName).AddressList[0].ToString();

                        string SSQL = "";

                        SSQL = "";
                        SSQL = "select max(count) as count from [" + SessionPayroll + "]..ADVDetails where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Empno='" + txtTokenNo.Text + "'";
                        DataTable dt_AdCount = new DataTable();
                        dt_AdCount = objdata.RptEmployeeMultipleDetails(SSQL);

                        string AdvCount = "0";
                        if (dt_AdCount.Rows.Count > 0)
                        {
                            AdvCount = dt_AdCount.Rows[0]["count"].ToString();
                        }

                        SSQL = "";
                        SSQL = "select * from [" + SessionPayroll + "]..advDetails where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and Convert(date,IssueDate,103)<=Convert(date,'" + txtdate.Text + "',103) and Existingcode='" + txtTokenNo.Text + "' and status='Pending'";
                        DataTable dt_checkadv = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_checkadv.Rows.Count > 0)
                        {

                            SSQL = "Select * from [" + SessionPayroll + "]..Advance_Credit_Debit where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and TransDate='" + txtdate.Text + "' and MachineID='" + txtTokenNo.Text + "' and isnull(AmtMin,'0')>'0'";
                            DataTable dt_check = new DataTable();
                            dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt_check.Rows.Count > 0)
                            {
                                SSQL = "Delete from [" + SessionPayroll + "]..Advance_Credit_Debit where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and TransDate='" + txtdate.Text + "' and MachineID='" + txtTokenNo.Text + "' and isnull(AmtMin,'0')>'0'";
                                objdata.RptEmployeeMultipleDetails(SSQL);
                            }

                            SSQL = "";
                            SSQL = "Insert into [" + SessionPayroll + "]..Advance_Credit_Debit (Ccode,Lcode,MachineID,ExistingCode,Wages,AmtAdd,AmtMin,Status,TransDate,AddIP,AddHostName,AddUserName,ModifyIP,ModifyHostName,ModifyUser,Month,FinYear,Count)";
                            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtTokenNo.Text + "','" + ddlExistingCode.SelectedItem.Text + "',";
                            SSQL = SSQL + "'" + ddlWagesType.SelectedItem.Text + "','0.00','" + txtAmount.Text + "','Pending','" + Convert.ToDateTime(txtdate.Text).ToString("dd/MM/yyyy") + "',";
                            SSQL = SSQL + "'','','','" + IPAddress + "','" + HostName + "','" + Session["UserId"].ToString() + "','" + Convert.ToDateTime(txtdate.Text).ToString("MMMM") + "','" + currentYear + "_" + (currentYear + 1) + "','"+Convert.ToInt32(AdvCount)+"')";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Saved Successfully....!');", true);
                        }else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Advance not Present on this Date=" + txtdate.Text + "! Please check the Date');", true);
                        }


                        //Status Check and updated
                        string Balance_Amt = "0";
                        SSQL = "";
                        SSQL = "select isnull(sum(AmtAdd),'0')-isnull(sum(AmtMin),'0') as Balance from [" + SessionPayroll + "]..Advance_Credit_Debit where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and MachineID='" + txtTokenNo.Text + "'";
                        SSQL = SSQL + " group by MachineID";
                        SqlConnection cn = new SqlConnection(constr);
                        SqlCommand cmd = new SqlCommand(SSQL, cn);
                        cn.Open();
                        Balance_Amt = cmd.ExecuteScalar().ToString();
                        cn.Close();
                        if (Convert.ToDecimal(Balance_Amt) == 0)
                        {
                            SSQL = "";
                            SSQL = "Update [" + SessionPayroll + "]..Advance_Credit_Debit set Status='Completed' where MachineID='" + txtTokenNo.Text + "' and Status='Pending'";
                            objdata.RptEmployeeMultipleDetails(SSQL);

                            SSQL = "";
                            SSQL = "Update [" + SessionPayroll + "]..AdvDetails set ModifyDate=Convert(varchar,getdate(),105),Balance='0.0',Status='Completed' where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and EmpNo='" + txtTokenNo.Text + "' and Status='Pending'";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                        else if (Convert.ToDecimal(Balance_Amt) > 0)
                        {
                            SSQL = "";
                            SSQL = "Update [" + SessionPayroll + "]..Advance_Credit_Debit set Status='Pending' where MachineID='" + txtTokenNo.Text + "' and Count='" + AdvCount + "'";
                            objdata.RptEmployeeMultipleDetails(SSQL);

                            SSQL = "";
                            SSQL = "Update [" + SessionPayroll + "]..AdvDetails set ModifyDate=Convert(varchar,getdate(),105),Balance='" + Convert.ToDecimal(Balance_Amt) + "',Status='Pending' where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and EmpNo='" + txtTokenNo.Text + "' and Status='Pending'";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                        btnClear_Click(sender, e);
                    }
                    //string Completed = "";
                    //mydate = Convert.ToDateTime(txtdate.Text);

                    //int Mont = Convert.ToInt32(mydate.Month);
                    //#region MonthName
                    //DateTime Md = new DateTime();


                    //switch (Mont)
                    //{
                    //    case 1:
                    //        MyMonth = "January";
                    //        break;

                    //    case 2:
                    //        MyMonth = "February";
                    //        break;
                    //    case 3:
                    //        MyMonth = "March";
                    //        break;
                    //    case 4:
                    //        MyMonth = "April";
                    //        break;
                    //    case 5:
                    //        MyMonth = "May";
                    //        break;
                    //    case 6:
                    //        MyMonth = "June";
                    //        break;
                    //    case 7:
                    //        MyMonth = "July";
                    //        break;
                    //    case 8:
                    //        MyMonth = "August";
                    //        break;
                    //    case 9:
                    //        MyMonth = "September";
                    //        break;
                    //    case 10:
                    //        MyMonth = "October";
                    //        break;
                    //    case 11:
                    //        MyMonth = "November";
                    //        break;
                    //    case 12:
                    //        MyMonth = "December";
                    //        break;
                    //    default:
                    //        break;
                    //}
                    //#endregion
                    //IncrementMonths = (Convert.ToInt32(inc_Month) + 1).ToString();
                    //decrementMonths = (Convert.ToDecimal(dec_month) - 1).ToString();
                    //string bal = (Convert.ToDecimal(txtBalanceAmt.Text) - Convert.ToDecimal(txtAmount.Text)).ToString();
                    //if (Convert.ToDecimal(txtAmount.Text) == Convert.ToDecimal(txtBalanceAmt.Text))
                    //{
                    //    Completed = "Y";

                    //    Query = "Insert Into [" + SessionPayroll + "].. Advancerepayment (EmpNo,TransDate,Months,Amount,AdvanceId,Ccode,Lcode) ";
                    //    Query = Query + " values ('" + txtTokenNo.Text + "',convert(datetime,'" + mydate + "',105),";
                    //    Query = Query + "'" + MyMonth + "','" + txtAmount.Text + "','" + id + "','" + SessionCcode + "','" + SessionLcode + "')";
                    //    objdata.RptEmployeeMultipleDetails(Query);

                    //    Query = "  Update [" + SessionPayroll + "]..AdvancePayment Set BalanceAmount='" + bal + "',";
                    //    Query = Query + " IncreaseMonth='" + IncrementMonths + "',ReductionMonth='0', ";
                    //    Query = Query + " Completed='" + Completed + "',Modifieddate=convert(datetime,'" + mydate + "',105)";
                    //    Query = Query + " where ID='" + id + "' and EmpNo='" + txtTokenNo.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //    objdata.RptEmployeeMultipleDetails(Query);

                    //    SaveFlag = true;
                    //}
                    //else
                    //{
                    //    Completed = "N";
                    //    Query = "Insert Into [" + SessionPayroll + "].. Advancerepayment (EmpNo,TransDate,Months,Amount,AdvanceId,Ccode,Lcode,AdvanceType) ";
                    //    Query = Query + " values ('" + txtTokenNo.Text + "',convert(datetime,'" + mydate + "',105),";
                    //    Query = Query + "'" + MyMonth + "','" + txtAmount.Text + "','" + id + "','" + SessionCcode + "','" + SessionLcode + "','" + rbtnType.SelectedItem.Text + "')";
                    //    objdata.RptEmployeeMultipleDetails(Query);

                    //    Query = "  Update [" + SessionPayroll + "]..AdvancePayment Set BalanceAmount='" + bal + "',";
                    //    Query = Query + " IncreaseMonth='" + IncrementMonths + "',ReductionMonth='" + decrementMonths + "', ";
                    //    Query = Query + " Completed='" + Completed + "',Modifieddate=convert(datetime,'" + mydate + "',105)";
                    //    Query = Query + " where ID='" + id + "' and EmpNo='" + txtTokenNo.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    //    objdata.RptEmployeeMultipleDetails(Query);

                    //    SaveFlag = true;
                    //}
                }
                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    clear();
                    DataTable dtempty = new DataTable();
                    ddlExistingCode.DataSource = dtempty;
                    txtEmpName.Text = "";
                    txtTokenNo.Text = "";
                    Load_Wages();
                    
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
        txtTokenNo.Text = "";
        DataTable dtempty = new DataTable();
        ddlWagesType.ClearSelection();
        ddlExistingCode.DataSource = dtempty;
        ddlExistingCode.DataBind();
        txtEmpName.Text = "";
    }
    public void SettingServerDate()
    {
        Datetime_ser = DateTime.Now.ToString("dd/MM/yyyy");
        
        txtdate.Text = Datetime_ser;
    }
    public void clear()
    {
        txtAmount.Text = "";
        txtdate.Text = "";
        txtBalanceAmt.Text = "";
        txtDesignation.Text = "";
        
        SettingServerDate();
    }

    protected void rbtnType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Repay_Type();
    }

    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtAmount.Text) > Convert.ToDecimal(txtBalanceAmt.Text))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Amount is should be Lesser than Balance Amount');", true);
            return;
        }
    }
}
