﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptAdvaceEmpReport.aspx.cs" Inherits="RptAdvaceEmpReport" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <script src="assets/js/master_list_jquery.min.js"></script>
            <script src="assets/js/master_list_jquery-ui.min.js"></script>
            <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
            <script>
                $(document).ready(function () {
                    $('#example').dataTable();
                });
            </script>


            <script type="text/javascript">
                //On UpdatePanel Refresh
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                if (prm != null) {
                    prm.add_endRequest(function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            $('#example').dataTable();
                            $('.select2').select2();
                            $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                autoclose: true
                            });
                        }
                    });
                };

            </script>

            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Advance</a></li>
                    <li class="active">Advance Report</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Advance Report</h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Salary Advance Report</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" ID="ddlWagesType" class="form-control select2" Style="width: 100%;"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlWagesType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Employee No</label>
                                                <asp:DropDownList runat="server" ID="ddlMacineId" class="form-control select2" Style="width: 100%;"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->


                                        <!-- begin row -->


                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnEmpAdv" Text="Employee Wise"
                                                    class="btn btn-success" OnClick="btnEmpAdv_Click" />
                                                <asp:Button runat="server" ID="btnExcel" Text="Excel" class="btn btn-primary" OnClick="btnExcel_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger" />
                                                <asp:Button ID="btnReport" Visible="false" class="btn btn-warning" runat="server" Text="Report" OnClick="btnReport_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:GridView ID="GVAdvance" runat="server" AutoGenerateColumns="false" CellSpacing="50">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Machine ID</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="DeptName" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>OP.BAL</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="OpenBalance" runat="server" Text='<%# Eval("Open") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                        <asp:TemplateField>
                                                        <HeaderTemplate>PAID</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Paid" runat="server" Text='<%# Eval("PAID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>RECD</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Received" runat="server" Text='<%# Eval("RECD") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>CL.BAL</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Close" runat="server" Text='<%# Eval("Close") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                 
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

